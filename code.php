<?php

// [] object from classes

// public - access modifier

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }
    // Getter
    public function getName(){
        return $this->name;
    }
    //Setter
    public function setName($name){
        $this->name = $name;
    }
    public function getFloors(){
        return $this->floors;
    }
    //Setter
    public function setFloors($floors){
        $this->floors = $floors;
    }
    public function getAddress(){
        return $this->address;
    }
    //Setter
    public function setAddress($address){
        $this->address = $address;
    }
    //setter
    public function printName(){
        return "The name of the building is $this->name";
    }

    // Class methods

}

// [] Inheritance and Polymorphism

class Condominium extends Building {
    public function printName(){
        return "The name of the condominium is $this->name";
}
}
// object creation from a class
$building = new Building('Caswynn Building', 8, 'Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, "Makati City, Philippines");